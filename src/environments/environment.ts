// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyDoYQZaFub9ZRpdguqnyy6z9X5lyR8nye4",
    authDomain: "notesminiprojet.firebaseapp.com",
    projectId: "notesminiprojet",
    storageBucket: "notesminiprojet.appspot.com",
    messagingSenderId: "198904983743",
    appId: "1:198904983743:web:4791533f7772723d6c160a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
