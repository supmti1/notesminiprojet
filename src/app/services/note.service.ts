import { Injectable } from '@angular/core';
import { Firestore, addDoc, collection, collectionData, deleteDoc, doc, docData, updateDoc } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
export interface Note{
  Id:number;
  Title:string;
  Body:string;
}

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private firestore: Firestore) { }

  getNoteById(id: any):Observable<Note>{
    const note = doc(this.firestore,`notes/${id}`);
    return docData(note) as Observable<Note>;
  }
  addNote(note:Note){
    const noteColl = collection(this.firestore,'notes');
    return addDoc(noteColl,note)
  }
  deleteNote(note:Note){
    const noteDoc= doc(this.firestore,`notes/${note.Id}`);
    return deleteDoc(noteDoc);
  }
  setNote(note:Note){
    const noteDoc= doc(this.firestore,`notes/${note.Id}`);
    return updateDoc(noteDoc,{title:note.Title ,body:note.Body });

  }
  getAllNotes():Observable<Note[]>{
    const notes = collection(this.firestore,'notes');
    return collectionData(notes,{idField:'createdBy'}) as Observable<Note[]>;
  }
}
