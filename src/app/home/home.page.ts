import { Component } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { NoteService } from '../services/note.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  notes!: any[];
  constructor(private dataService:NoteService,private alertController:AlertController,
    private modalController: ModalController
    ) {
    this.dataService.getAllNotes().subscribe(response=>{
      console.log(response);
      this.notes=response;
    })
  }
  async addNote(){
   const alert=await this.alertController.create({
      header: 'Ajoute Note',
      inputs:
      [
        {
          name:'Title',
          placeholder:'Entre tite de la note',
          type:'text'
        },
        {
          name:'Body',
          placeholder:'Entre content de la note',
          type:'text'
        }
        
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Annuler clicked');
          },
        },
        {
          text: 'Ajouter',
          handler: (data) => {
             if (data.title.trim() !== '') {
              this.dataService.addNote({
                Title: data.title, Body: data.body,
                Id: Math.floor(Math.random() * 100) + 1
              });

           }
          },
        },
      ],
      
    });
    await alert.present();
  }
  async showNote( note: any){
    const modal=await this.modalController.create({
      component:ModalController,
      componentProps:{id:note.id},
    });
    modal.present();
  }

}
