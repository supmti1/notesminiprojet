import { Component, Injectable, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { Note, NoteService } from '../services/note.service';


@Component({
  selector: 'app-desc',
  templateUrl:'./desc.page.html',
  styleUrls:['./desc.page.sccs']

})
export class DescriptionPage implements OnInit {
    @Input() id:string | undefined;
    note:Note | undefined;
  constructor(private data:NoteService, private modalController:ModalController,private toastController:ToastController ) { }
    ngOnInit(): void {
        this.data.getNoteById(this.id).subscribe(response=>{
            this.note=response
        });
    }
    async setNote(){
        this.data.setNote(this.note as Note);
        const toast = await this.toastController.create({
            message:'Note été modifier',
            duration:2000,
        });
        toast.present();
    }
    async deleteNote(){
        this.data.deleteNote(this.note as Note);
        this.modalController.dismiss();
        const toast = await this.toastController.create({
            message:'Note été supprimer',
            duration:2000,
        });
        toast.present();
    }
}
