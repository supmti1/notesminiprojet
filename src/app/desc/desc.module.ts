import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DescriptionPage } from './desc.page';

import { DescriptionPageRoutingModule } from './desc-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescriptionPageRoutingModule
  ],
  declarations: [DescriptionPage]
})
export class HomePageModule {}
